# Getting started with EpiNav coding

This small tutorial will guide you into writing your first CLI module for EpiNav.

## Build EpiNav

### Requirements

#### Git
A good way to have `git` on Windows is by installing the full version of [`cmder`](cmder).
Add the `git` bin folder to your PATH environment variable: `path_to_cmder_dir\vendor\git-for-windows\bin`.

#### CMake
Download and install [CMake 3.3](cmake).

#### Visual Studio
Download and install [Visual Studio Community 2013 with Update 5](vs2013).

#### Qt
Download [Qt](qt).
Install only `msvc2013 64-bit OpenGL` from Qt 5.4, and add the installation directory to your PATH environment variable.

[cmder]: http://cmder.net/
[cmake]: https://cmake.org/files/v3.3/cmake-3.3.1-win32-x86.exe
[vs2013]: https://my.visualstudio.com/Downloads?q=visual%20studio%202013&wt.mc_id=o~msft~vscom~older-downloads
[epinav binaries folder]: https://drive.google.com/open?id=0Bzkp1J5JcafYaWo4QVJLelkzSGc
[qt]: https://www.qt.io/download-thank-you


### Get source code
```shell
git clone git@gitlab.com:KCL-BMEIS/Surgical-guidance/EpiNav.git
```

### Configure and generate
 1. Open CMake 3.3
 2. Specify where the source code is
 3. Specify where you want to build. This **must** be a very short path, e.g. `C:\EpiNav-build`
 4. Click on `Configure`. Use `Visual Studio 12 2013 Win64` as generator
 5. Search `qt` and make sure that CMake found the Qt you installed and not the one in Anaconda or Miniconda. In that case, put the Qt folder before Anaconda stuff in your PATH list and restart CMake
 6. Click on `Generate` and exit.

### Build
 1. Go to your build directory and open `NIFTK-superbuild.sln`
 2. Click on `BUILD -> Build Solution`
 3. Wait some hours


## Create a CLI module

### Requirements

#### EpiNav
Download and install EpiNav using one of [Google Drive binaries](epinav binaries folder).
Using the installed version instead of the built will be easier for testing your module.

### Create module files
  1. Go to `[path_to_source_code]\MITK\Applications`
  2. Clone one existing module, for example `TransformElectrodes`. Rename it to your module's name, for example `MyAwesomeModule`
  3. Edit `[path_to_source_code]\MITK\Applications\CMakeLists.txt` by adding your module right under the `if(SlicerExecutionModel_FOUND)` line: `add_subdirectory(MyAwesomeModule)`
  4. Rename the `xml.in` and `.cxx` files appropriately in `[path_to_source_code]\MITK\Applications\MyAwesomeModule`
  5. Edit the `NAME` in `[path_to_source_code]\MITK\Applications\MyAwesomeModule\CMakeLists.txt`
  6. Edit your XML and CXX files with awesome code

### Configure and generate
  1. Open CMake 3.3
  2. Specify where the source code is
  3. Specify a new build directory: `C:\EpiNav-build\NifTK-build`
  4. Configure and generate

### Build module
  1. Open `C:\EpiNav-build\NifTK-build\NIFTK.sln`
  2. In the `Solution Explorer`, go to `Command line Apps` and right-click on your module, for example `niftkMyAwesomeModule`
  3. `Project only -> Rebuild Only niftkMyAwesomeModule`

### Run module
  1. Open the installed version of EpiNav
  2. Go to `Window -> Preferences...`
  3. Go to `Command Line Modules` and add your executable (e.g. `C:\EpiNav-build\NifTK-build\bin\Release\niftkMyAwesomeModule.exe`) to the `additional modules` list
  4. Your module should be available in the `Command Line Modules` plugin of EpiNav!
